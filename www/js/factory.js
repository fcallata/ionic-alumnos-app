var base = 'http://localhost:8000';

angular.module('Alumnos-App.factory', [])

.factory('Loader', ['$ionicLoading', '$timeout',function($ionicLoading, $timeout) {
	var LOADERAPI = {
		showLoading: function(text) {
			text = text || 'Loading...';
			$ionicLoading.show({
				template: text
			});
		},

		hideLoading: function() {
			$ionicLoading.hide();
		},

		toggleLoadingWithMessage: function(text, timeout) {
			$rootScope.showLoading(text);
			$timeout(function() {
				$rootScope.hideLoading();
			}, timeout || 3000);
		}
	};
	return LOADERAPI;
}])

.factory('LSFactory', [function() {

	var LSAPI = {
		clear: function() {
			return localStorage.clear();
		},

		get: function(key) {
			return JSON.parse(localStorage.getItem(key));
		},

		set: function(key, data) {
			return localStorage.setItem(key,
			JSON.stringify(data));
		},

		delete: function(key) {
			return localStorage.removeItem(key);
		},

		getAll: function() {
			var books = [];
			var items = Object.keys(localStorage);
			for (var i = 0; i < items.length; i++) {
				if (items[i] !== 'user' || items[i] != 'token') {
					books.push(JSON.parse(localStorage[items[i]]));
				}
			}
			return books;
		}
	};
	return LSAPI;
}])

/*
AUTENTICACION

TOKEN INTERCEPTOR
*/

.factory('AlumnosAppFactory', ['$http', function($http) {
	var perPage = 30;
	var API = {
		get: function(page) {
			//return $http.get(base + '/api/v1/books/' + page + '/'+ perPage);
			var ret = $http.get(base + '/user/').then(function(response){
				console.log('verdadero');
			}, function(error){
				console.log('eror-->' + error);
			});
		return ret; 
		}
	};
	return API;
}])

.factory('UsuariosAppFactory', ['$http', function($http) {
	var perPage = 30;
	var API = {
		get: function(page) {
			//return $http.get(base + '/api/v1/books/' + page + '/'+ perPage);
			var ret = $http.get(base + '/user/').then(function(response){
				console.log('verdadero');
			}, function(error){
				console.log('eror-->' + error);
			});
		return ret; 
		}
	};
	return API;
}])

