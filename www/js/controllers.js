angular.module('Alumnos-App.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('BrowseCtrl', ['$scope', 'AlumnosAppFactory', 'LSFactory','Loader', function($scope, AlumnosAppFactory, LSFactory, Loader) {
    Loader.showLoading();
    // support for pagination
    var page = 1;
    $scope.books = [];
    var books = LSFactory.getAll();
    // if books exists in localStorage, use that instead of making a call
    if (books.length > 0) {
        $scope.books = books;
        Loader.hideLoading();
        console.log('entro en el if');
    } else {
      console.log('entro en el else');
        AlumnosAppFactory.get(page).success(function(data) {
// process books and store them
// in localStorage so we can work with them later on,
// when the user is offline
          processBooks(data.data.books);
          $scope.books = data.data.books;
          $scope.$broadcast('scroll.infiniteScrollComplete');
          Loader.hideLoading();
      }).error(function(err, statusCode) {
        Loader.hideLoading();
        Loader.toggleLoadingWithMessage(err.message);
      });
    }

    function processBooks(books) {
    LSFactory.clear();
      // we want to save each book individually
      // this way we can access each book info. by it's _id
      for (var i = 0; i < books.length; i++) {
        LSFactory.set(books[i].usuario_id, books[i]);
        console.log('valor: '+books[i].usuario_id);
      };
    }
}])

.controller('UsuariosCtrl', function($scope, $http){    
    $scope.data = "";
    $scope.model = {search: ''};
    //, {params: {search: $scope.model.search}
    $scope.search = function(){
      $http.get('http://localhost:3000/user/search/'+ $scope.model.search).success(function(data) {
        // Get dynamic data from JSON file
          //var alumno = JSON.parse(data);
          $scope.data = data;
          //console.log("valor de data: "+alumno.name);
          //$scope.tabs = data;
        }).error(function(status) {
        // if error, show status
        console.log("valor de status: "+ status);
        })
    }
    
})
//$httpParamSerializerJQLike
.controller('AltaUsuarioCtrl', function($scope, $http){
    //$scope.data = JSON.stringfy($scope.user);*/
    $scope.user = {name:'',lastname:'',email:''};
    
    $scope.alta = function(){  
      $http.post('http://localhost:3000/user/', $scope.user ).success(function(data) {
        // Get dynamic data from JSON file
          //var alumno = JSON.parse(data);
          //$scope.data = data;
          console.log("exito: " + data.status);
          //$scope.tabs = data;          
        }).error(function(status) {
        // if error, show status
          console.log("valor de status: "+ status);
      })
    }
})

.controller('ListaSuperCtrl', function($scope, $http){    
    $scope.data = "";
    $scope.model = {search: ''};
    //, {params: {search: $scope.model.search}
    $scope.search = function(){
      //$http.get('http://localhost:3000/supermarket'+ $scope.model.search).success(function(data) {
        $http.get('http://localhost:3000/supermarket').success(function(data) {
        // Get dynamic data from JSON file
          //var alumno = JSON.parse(data);
          $scope.data = data;
          //console.log("valor de data: "+alumno.name);
          //$scope.tabs = data;
        }).error(function(status) {
        // if error, show status
        console.log("valor de status: "+ status);
        })
    }
    
});

/*.controller('UsuariosCtrl', function(){
    $resource('http://127.0.0.1:3000/user/',{},{
      'query':{method:'GET', isArray:true, responseType:'json'}
    })
});*/